# grdlss

> grdlss is a simple less grid framework which provides simple access to build your own grids


This is not necessarily a helpful thing (mainly because you can only define one grid, without using js or php) but it shows some cool less magic and i wanted to build something that is like the oldscool tables but better (which worked pretty well in my head).

### technologies used

* [LESS](http://lesscss.org)
* HTML5

### install

Link the compiled **grdlss.css** file to your html file:
```
<link rel="stylesheet" href="css/grdlss.css" type="text/css">
```

### how to use

You have to setup the grd.less file properly.
Starting at line 19 you can find this:
```
/**
 * Variables
 *
 * !note Just the variables for grdlss
 * !note This is the place where you can change stuff
 */
@grdlss-height         : 200px;
@grdlss-width          : @grdlss-height;
@grdlss-padding-default: 10px;
@grdlss-per-line       : 3;
@grdlss-border-style   : solid 2px #999;
@grdlss-highlight-color: #f1f1f1;
```

This is where you can make changes to your grid.

***explanation of variables***

variable name | explanation 
 --- | ---
***@grdlss-height*** | the height of a grid element
***@grdlss-width*** | the width of a grid element, you can use @grdlss-height to easily make the grid elements square
***@grdlss-padding-default*** | the default inner grid element padding
***@grdlss-per-line*** | how much grid elements shoud displayed in one row
***@grdlss-border-style*** | grid wrapper border style
***@grdlss-highlight-color*** | default grid element highlighting color

***After changing the variables you need to compile the following***
> ***less/grd.less > css/grdless.css***

Done? Alright now you can add some grd markup to the html file which should put out the grid.

The markup is pretty simple. Just start with a default one:

```
<section class='grdlss'> // the grdlss wrapper
            <section>Grid element 1</section> // a grdlss data element
            <section>Grid element 2</section>
            <section>Grid element 3</section>
            <section>Grid element 4</section>
            <section>Grid element 5</section>
            <section>Grid element 6</section>
</section>
```

### additional helpers

There some more classes you can use to customize your grid.

class name | applied to | explanations 
--- | --- | ---
grdlss-double | ```<section class='...'>``` (grid element) | doubles the grid element size so it is practicaly a colspan='2'
grdlss-whole-width | ```<section class='...'>``` (grid element) | stretch the grid element size to the line maximum
grdlss-center | ```<section class='grdlss ...'>``` (grid wrapper) | align the grid to the center of the page
grdlss-border | ```<section class='grdlss ...'>``` (grid wrapper) | adds a border to the grid
grdlss-overflow | ```<section class='grdlss ...'>``` (grid wrapper) | enable overflow for grid elements
grdlss-padding-on | ```<section class='grdlss ...'>``` (grid wrapper) | adds an grid element padding
grdlss-highlight-```[...]``` | ```<section class='grdlss ...'>``` (grid wrapper) | adds different background color for every ```[odd]``` or ```[even]``` grid element
